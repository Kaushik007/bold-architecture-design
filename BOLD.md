# **CODEBASE REFACTORING USING SOLID ARCHITECTURE**
## Introduction
**SOLID** is an acronym for 5 important design principles in OOPs(Object Oriented Programming System).In this world **SOLID** contains 5 letters and each of this 5 letters have their meaning which is the foundation of design principle.
#Point for Refactoring The Code
As I mentioned above there are certain meanings of all the world. So let's starts with that.
<br>
####1. S :#### 
S stands for Single Responsiblity Principle.It's mean that every class have a responsiblity of a single part.
``` java
public class Draw {
    public void circle() {}
    public double rectangle() {}
    public void square() {}
}
````
The Draw class has three separate responsibilities: rectangle, circle, and square. By applying SRP(Single Responsibility Principle), we can separate the above class into three classes with separate responsibilities.
<br>

####2. O :#### 
O stands for Open-Closed Principle. In oops there is a concept of polymorphism so we have to sure that our code maintains the law of open-close principle by utilizing inheritance and interfaces.
 
So starting with and example:
``` java
public class AreaCalculation {
    public double calculatingArea(Area A){
      if(A instanceOf circle){
        return A.getValue()*2*Math.PI;
      }if(A instanceOf Rectangle){
        return A.getValue()*2;
      }
}
````
If I want to add another subclass we have to modify the above class by adding an if statement which violates the rules of the Open-close principle. So the better way to approach is:
``` java
public class Area {
    public double calculatingArea(){....}
}
public class Circle extands Area{
      public double calculatingArea(){
        return A.getValue()*2*Math.PI;
      }
}
public class Rectangle extands Area{
      public double calculatingArea(){
        return A.getValue()*2;
      }
}
````
####3. L :####
L stands for Liskov Substitution Principle.This principle says that derived class must substitutable for their base classes.
```java
public class Vehicle {
    private double milage;
    private String typeOfEngine ;
    public void setMilage(double m) { 
      milage = m; 
      }
    public void setTypeOfEngine(String e){
      typeOfEngine= e; 
    }
    ...
}
public class Car extends Vehicle {
    public void setMilage(double m) {
        super.setMilage(m);
        super.setTypeOfEngine(e);
    }
    public void setTypeOfEngine(String e) {
        super.setMilage(m);
        super.setTypeOfEngine(e);
    }
}
````
In this example the class is violating the Liskov Substitution Principle because Vehicle class is not replaced by Car class.
<br>
####4. I :####
I stand for Interface Segregation Principle. In this principle, The clients should not be forced to depend upon interface members that are not required.

```java
public interface Draw{
  public void line();
  public void curve();
  public void density();
}
public class Semicircle implements Draw{

  //Can be implemented
  public void line(){...}
  public void curve(){....}

  // can not be implemented
  public void density(){....}
}
````
It doesn't make sense for a semicircle class to implement the density() method as a semicircle does not have density. So to resolve this issue this interface has to be broken down into small interfaces so that no class is forced to implement it.
<br>
####5. D:####
D stands for Dependency Inversion Principle. In this principle we should depend on abstract classes and interfaces instead of classes and the abstraction should not be dependent upon details but the details should depend upon abstractions.

```java
public class Truck{
  private Suspension suspension;
  public Truck(Suspension s){
    Suspension=s;
  }
  public void capacity(){
    suspension.loadFactor();
  }
}
public class Suspension{
  public void loadFactor(){...}
}
````
In this code, if we want to add another suspension type like helical spring then we want to change the Truck class. So we add an Interface.
```java
public interface SuspensionInterface{
  public void loadFactor();
}
````
now we can use any kind of Suspension in this Truck Class.
```java
public class Truck{
  private SuspensionInterface suspension;
  public Truck(SuspensionInterface s){
    Suspension=s;
  }
  public void capacity(){
    suspension.loadFactor();
  }
}
public class HelicalSpring implements SuspensionInterface{
  public void loadFactor(){...}
}
public class LeafSpring implements SuspensionInterface{
  public void loadFactor(){...}
}
````
It resolves the code and also increases the flexibility of the code.
<br>

##Reference##
[SOLID Principles: Explanation and examples](https://itnext.io/solid-principles-explanation-and-examples-715b975dcad4)
